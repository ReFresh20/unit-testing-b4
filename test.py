import unittest
from unittest.case import addModuleCleanup
from functions import add


class test_add(unittest.TestCase):
    def test_equal(self):
        self.assertEqual(addModuleCleanup(int, "2"), "4")

    def test_false(self):
        self.assertNotEqual(add(2, 2), 8)

    def test_type(self):
        self.assertEqual(int(add(2, 2)), type)

    if __name__ == '__main__':
        unittest.main()
